[[ $- != *i* ]] && return
export PATH=$PATH:/home/potsbaker/.scripts/
PS1='\u@\h [\W] \$ '
alias q='exit'
alias ls='ls --color=always'
alias sudo='doas'